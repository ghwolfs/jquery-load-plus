/*! 
 * jquery-load-plus v1.1 MIT license
 * @author Ghwolf
 * @see https://gitee.com/ghwolfs/jquery-load-plus
 */
(function() {
	if (typeof jQuery == 'undefined' || jQuery.loadplus) {
		return;
	}
	var rscriptType = (/^$|\/(?:java|ecma)script/i);
	var $ = jQuery;
	var resourceLoadCache = [];
	var isOpera = typeof opera !== 'undefined' && opera.toString() === '[object Opera]';
	var htmlCache = {};
	var allowCache = !(window.nocache === true);
	var $head = $('head');
	
	// 将两个url合并，自动处理"/"，如果第二个是http*开头，则直接返回url2
	function urlMerge(url1, url2) {
		if (url2.indexOf('http://') == 0 || url2.indexOf('https://') == 0) {
			return url2;
		}
		var f1 = url1.lastIndexOf('/') == url1.length - 1;
		var f2 = url2.indexOf('/') == 0;
		if (f1 && f2) {
			return url1 + url2.substring(1);
		} else if (!f1 && !f2) {
			return url1 + '/' + url2;
		} else {
			return url1 + url2;
		}
	}
	/**
	 * 加载一个script并返回Deferred延迟对象，加载完毕后会执行回调
	 * @param {Object} scriptNode script节点对象
	 */
	function loadScript(scriptNode) {
		var node = document.createElement('script');
		node.charset = "UTF-8";
		node.src = scriptNode.src;
		if (scriptNode.defer) {
			node.defer = true ;
		}
		if ($(scriptNode).attr('async') !== void 0) {
			node.async = scriptNode.async;
		} else {
			// 同步执行避免后续脚本过早执行
			node.async = false ;
		}
		
		$head[0].appendChild(node);
		
		var def = $.Deferred();
		
		if (node.attachEvent && !(node.attachEvent.toString && node.attachEvent.toString().indexOf('[native code') < 0) && !isOpera) {
			node.attachEvent('onreadystatechange', function(e) {
				def.resolve(scriptNode.url,e);
			});
		} else {
			node.addEventListener('load', function(e) {
				def.resolve(scriptNode.url,e);
			}, false);
		}
		return def ;
	}

	$.fn.loadplus = function(url, params, option) {
		if (typeof url !== 'string') {
			throw 'url 必须是字符串，但现在却是：' + typeof url + ', url = ' + url;
		}

		if (typeof params === 'function') {
			option = {
				complete: params
			}
			params = void 0;
		} else if (option) {
			if (typeof option === 'function') {
				option = {
					complete: option
				}
			}
		} else {
			option = {};
		}

		var selector,
			response,
			self = this,
			off = url.indexOf(' ');

		if (self.length > 0) {
			if (self.length !== 1) {
				self = self.eq(0);
			}
			if (off >= 0) {
				selector = url.slice(off, url.length);
				url = url.slice(0, off);
			}

			var urlBasicPrefix = url.substring(0, url.lastIndexOf('/'));

			function successCallback(responseText) {
				response = arguments;
				var roots = $('<div>').append($.parseHTML(responseText, true)),
					// 最终渲染的dom，不含script(type非script的除外)
					selfAppendDoms = [],
					// 非script、link、style节点jquery对象集合
					callThis = [],
					// 需要进行堵塞执行的script脚本
					scripts = [];

				if (selector) {
					roots = roots.find(selector);
				}

				// 避免读取到文本节点、注释节点等
				roots.children().each(function(i, root) {
					var $root = $(root);
					if (root.nodeName == 'SCRIPT') {
						if ($root.attr('src')) {
							var srcUrl = urlMerge(urlBasicPrefix, $root.attr('src'));
							$root.attr('src', srcUrl);
							if ($root.attr("cacheable") !== void 0) {
								if (resourceLoadCache.indexOf(root.src) == -1) {
									resourceLoadCache.push(root.src);
									scripts.push(root);
								}
							} else {
								scripts.push(root);
							}
						} else if (rscriptType.test(root.type)) {
							scripts.push(root);
						} else {
							selfAppendDoms.push(root);
							callThis.push(root);
						}
					} else if (root.nodeName == 'LINK') {
						var srcUrl = urlMerge(urlBasicPrefix, $root.attr('href'));
						$root.attr('href', srcUrl);
						if ($root.attr("cacheable") == void 0) {
							selfAppendDoms.push(root);
						} else {
							if (resourceLoadCache.indexOf(root.href) == -1) {
								resourceLoadCache.push(root.href);
								$head[0].appendChild(root);
							}
						}
					} else {
						selfAppendDoms.push(root);
						if (root.nodeName != 'STYLE') {
							callThis.push(root);
						}
					}
				});


				self.append(selfAppendDoms);

				callThis = $(callThis);

				
				scripts.reverse();
				
				function doLoadScript(){
					var node = scripts.pop();
					var defs = [];
					while(node) {
						if (node.src) {
							defs.push(loadScript(node));
						} else {
							$.when.apply($,defs).done(function(){
								var scriptText = node.text || '';
								new Function('jparams', scriptText).call(callThis, params);
								doLoadScript();
							});
							break ;
						}
						node = scripts.pop();
					}
				}
				doLoadScript();
				

			}
			
			function doLoad() {
				$.get(url, {}, $.noop, 'html')
					.done(function(responseText){
						if (allowCache) {
							htmlCache[url] = responseText;
						}
					})
					.done(successCallback)
					.fail(option.fail && function() {
						option.fail.apply(this, arguments);
					})
					.complete(option.complete && function(jqXHR, status) {
						option.complete.apply(this, response || [jqXHR.responseText, status, jqXHR]);
					});
			}
			if (allowCache) {
				var cacheData = htmlCache[url];
				if (cacheData) {
					successCallback(cacheData);
				} else {
					doLoad();
				}
			} else {
				doLoad();
			}

		}

		return this;
	}
})()
