@echo off

rem 你需要安装node以及uglify-js，然后才可以执行此命令进行构建
rem 如果你使用的是powershell，那么uglifyjs脚本可能会出现执行权限问题，你可以切换到cmd执行。

SET VERSION=1.1
SET NAME=jquery-load-plus
SET OUTPUT_DIR=dist
SET INPUT_FILE=src/%NAME%.js

if not exist %OUTPUT_DIR% (
	mkdir %OUTPUT_DIR%
)

uglifyjs %INPUT_FILE% -c --ie8 -m --comments /^!/ -o %OUTPUT_DIR%/%NAME%-%VERSION%.min.js
